from bitcoin.core.script import *

######################################################################
# These functions will be used by Alice and Bob to send their respective
# coins to a utxo that is redeemable either of two cases:
# 1) Recipient provides x such that hash(x) = hash of secret
#    and recipient signs the transaction.
# 2) Sender and recipient both sign transaction
#
# TODO: Fill these in to create scripts that are redeemable by both
#       of the above conditions.
# See this page for opcode documentation: https://en.bitcoin.it/wiki/Script

# This is the ScriptPubKey for the swap transaction
def coinExchangeScript(public_key_sender, public_key_recipient, hash_of_secret):
    return [ OP_IF, OP_HASH160, hash_of_secret, OP_EQUALVERIFY, public_key_recipient, OP_CHECKSIG, OP_ELSE
            , OP_2, public_key_sender, public_key_recipient, OP_2, OP_CHECKMULTISIG, OP_ENDIF

        # fill this in!
    ]

# This is the ScriptSig that the receiver will use to redeem coins
def coinExchangeScriptSig1(sig_recipient, secret):
    return [sig_recipient, secret, OP_1

        # fill this in!
    ]

# This is the ScriptSig for sending coins back to the sender if unredeemed
def coinExchangeScriptSig2(sig_sender, sig_recipient):
    return [OP_0, sig_sender, sig_recipient, OP_0

            ]
######################################################################

######################################################################
#
# Configured for your addresses
#
# TODO: Fill in all of these fields
#

alice_txid_to_spend     = "e050ed4b3f9dda13fcb065200bceea9c92a1edae38a7507446afeeeb14056e00"
alice_utxo_index        = 1
alice_amount_to_send    = 0.0003

bob_txid_to_spend       = "ca40a8f46aaf9a48fa63e4fdcc247adc211fc4f966fe6a74b8b601b4bb4f28c1"
bob_utxo_index          = 1
bob_amount_to_send      = 0.0003

# Get current block height (for locktime) in 'height' parameter for each blockchain (and put it into swap.py):
#  curl https://api.blockcypher.com/v1/btc/test3
btc_test3_chain_height  = 1896133

#  curl https://api.blockcypher.com/v1/bcy/test
bcy_test_chain_height   = 3156493

# Parameter for how long Alice/Bob should have to wait before they can take back their coins
## alice_locktime MUST be > bob_locktime
alice_locktime = 5
bob_locktime = 3

tx_fee = 0.0001

# While testing your code, you can edit these variables to see if your
# transaction can be broadcasted succesfully.
broadcast_transactions = False
alice_redeems = True

######################################################################
