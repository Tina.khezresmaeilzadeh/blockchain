from sys import exit
from bitcoin.core.script import *

from lib.utils import *
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from ex1 import P2PKH_scriptPubKey
from ex2a import Q2a_txout_scriptPubKey


######################################################################
# TODO: set these parameters correctly
amount_to_send = 0.0003 # amount of BTC in the output you're splitting minus fee
txid_to_spend = (
        '628fd1e29c2ca8752edc985d38350ee89b169bab994f6048f3a54af620d53427')
utxo_index = 0 # index of the output you are spending, indices start at 0
######################################################################
txin_scriptPubKey = [OP_2DUP, OP_LESSTHAN, OP_IF, OP_SWAP, OP_ENDIF, OP_SUB, 1595, 9610, OP_WITHIN]
# TODO: implement the scriptSig for redeeming the transaction created
# in  Exercise 2a.
txin_scriptSig = [10000, 5000]
######################################################################
txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

response = send_from_custom_transaction(
    amount_to_send, txid_to_spend, utxo_index,
    txin_scriptPubKey, txin_scriptSig, txout_scriptPubKey, network_type)
print(response.status_code, response.reason)
print(response.text)