from sys import exit
from bitcoin.core.script import *


from lib.config import *
from ex1 import P2PKH_scriptPubKey
from ex31a import Q3a_txout_scriptPubKey
from lib.utils_for_ex31 import *

######################################################################
# TODO: set these parameters correctly
amount_to_send = 0.0003 # amount of BTC in the output you're splitting minus fee
txid_to_spend = (
        'd6f4cbdd350ab2d911df0d2989600b8bc81fd3c86f3a66f1a04977da48f5d7e6')
utxo_index = 0# index of the output you are spending, indices start at 0
######################################################################

txin_scriptPubKey = Q3a_txout_scriptPubKey
######################################################################
# TODO: implement the scriptSig for redeeming the transaction created
# in  Exercise 2a.
txin_scriptSig = [5000, 5000]
######################################################################
txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

# As An Example:

sig_type = 1
if sig_type == 1:
    first_sig = faraz_private_key
    second_sig = None
    third_sig = None
    forth_sig = None
else:
    first_sig = ata_private_key
    second_sig = p1_private_key
    third_sig = p2_private_key
    forth_sig = p3_private_key





response = send_from_custom_transaction(
    amount_to_send, txid_to_spend, utxo_index,
    txin_scriptPubKey, sig_type, first_sig, second_sig, third_sig, forth_sig, txout_scriptPubKey, network_type)
print(response.status_code, response.reason)
print(response.text)
