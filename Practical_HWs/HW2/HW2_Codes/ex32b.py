from sys import exit
from bitcoin.core.script import *


from lib.config import *
from ex1 import P2PKH_scriptPubKey
from ex32a import Q3a_txout_scriptPubKey
from lib.utils_for_ex32 import *

######################################################################
# TODO: set these parameters correctly
amount_to_send = 0.00015 # amount of BTC in the output you're splitting minus fee
txid_to_spend = (
        'd6bedeb751227ee9a983faf75583ea4659ed986c9d602302b4917c921224d5b2')
utxo_index = 0# index of the output you are spending, indices start at 0
######################################################################

txin_scriptPubKey = Q3a_txout_scriptPubKey
######################################################################
# TODO: implement the scriptSig for redeeming the transaction created
# in  Exercise 2a.
txin_scriptSig = [5000, 5000]
######################################################################
txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

# As An Example:

txin = create_txin(txid_to_spend, utxo_index)


txin_scriptSig = [0, create_OP_CHECKSIG_signature(txin, txout_scriptPubKey, txin_scriptPubKey, p3_private_key),
                  create_OP_CHECKSIG_signature(txin, txout_scriptPubKey, txin_scriptPubKey, p2_private_key),
                  create_OP_CHECKSIG_signature(txin, txout_scriptPubKey, txin_scriptPubKey, p1_private_key), 0,
                  create_OP_CHECKSIG_signature(txin, txout_scriptPubKey, txin_scriptPubKey, faraz_private_key)]




response = send_from_custom_transaction(
    amount_to_send, txid_to_spend, utxo_index,
    txin_scriptPubKey, txin_scriptSig, network_type)
print(response.status_code, response.reason)
print(response.text)
