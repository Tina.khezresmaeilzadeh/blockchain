from sys import exit
from bitcoin.core.script import *

from lib.config import *
from ex1 import P2PKH_scriptPubKey
from ex33a import Q3a_txout_scriptPubKey
from lib.utils_for_ex33 import *

######################################################################
# TODO: set these parameters correctly
amount_to_send = 0.0002  # amount of BTC in the output you're splitting minus fee
txid_to_spend = (
    'e24e63a51e372bb125c2a1165e3bf9801746daa9f5b61622287ca80f5180cdb5')
utxo_index = 0  # index of the output you are spending, indices start at 0
######################################################################

txin_scriptPubKey = Q3a_txout_scriptPubKey
######################################################################
# TODO: implement the scriptSig for redeeming the transaction created
# in  Exercise 2a.

######################################################################
txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

# As An Example:


first_sig = faraz_private_key
second_sig = p1_private_key
third_sig = p2_private_key
forth_sig = p3_private_key

response = send_from_custom_transaction(
    amount_to_send, txid_to_spend, utxo_index,
    txin_scriptPubKey, first_sig, second_sig, third_sig, forth_sig, txout_scriptPubKey, network_type)
print(response.status_code, response.reason)
print(response.text)
