from sys import exit
from bitcoin.core.script import *

from lib.utils import *
from lib.config import (my_private_key, my_public_key, my_address,
                        faucet_address, network_type)
from ex5a import txout_scriptPubKey


def P2PKH_scriptPubKey(address):
    ######################################################################
    # TODO: Complete the standard scriptPubKey implementation for a
    # PayToPublicKeyHash transaction
    return [OP_DUP, OP_HASH160, address, OP_EQUALVERIFY, OP_CHECKSIG]


def birthday_scriptSig(txin, txout, txin_scriptPubKey):
    signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey, my_private_key)
    ######################################################################
    # TODO: Complete this script to unlock the BTC that was sent to you
    # in the PayToPublicKeyHash transaction.
    return [signature, my_public_key]


def send_from_custom_transaction(amount_to_send, txid_to_spend, utxo_index, txin_scriptPubKey, txout_scriptPubKey,
                                 network):
    txout = create_txout(amount_to_send, txout_scriptPubKey)
    txin = create_txin(txid_to_spend, utxo_index)
    txin_scriptSig = birthday_scriptSig(txin, txout, txin_scriptPubKey)

    new_tx = create_signed_transaction(txin, txout, txin_scriptPubKey,
                                       txin_scriptSig)
    return broadcast_transaction(new_tx, network)


if __name__ == '__main__':
    # TODO: set these parameters correctly
    amount_to_send = 0.00021  # amount of BTC in the output you're splitting minus fee

    txid_to_spend = (
        'b13949a1c89b9658c2aca52a6172755664aeae3973a363eb697c5a64bd9874e5')

    utxo_index = 0 # index of the output you are spending, indices start at 0
    ######################################################################

    txin_scriptPubKey = txout_scriptPubKey
    ######################################################################
    txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

    response = send_from_custom_transaction(amount_to_send, txid_to_spend, utxo_index, txin_scriptPubKey,
                                            txout_scriptPubKey, network_type)

    print(response.status_code, response.reason)
    print(response.text)
