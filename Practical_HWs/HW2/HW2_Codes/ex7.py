from bitcoin.core.script import *
from bitcoin.wallet import CBitcoinSecret, P2PKHBitcoinAddress

from lib.utils import *
from lib.config import *


def P2PKH_scriptPubKey(address):
    ######################################################################
    # TODO: Complete the standard scriptPubKey implementation for a
    # PayToPublicKeyHash transaction
    return [
        # fill this in!
        OP_DUP, OP_HASH160, address, OP_EQUALVERIFY, OP_CHECKSIG
    ]
    ######################################################################


def P2PKH_scriptSig(txin, txout, txin_scriptPubKey, private_key, public_key):
    signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             private_key)
    ######################################################################
    # TODO: Complete this script to unlock the BTC that was sent to you
    # in the PayToPublicKeyHash transaction.
    return [
        # fill this in!
        signature, public_key
    ]
    ######################################################################


def send_from_P2PKH_transaction(amount_to_send, txid_to_spend, utxo_index, p1_address, p2_address, p3_address, sender_private_key, network):
    sender_public_key = sender_private_key.pub
    sender_address = P2PKHBitcoinAddress.from_pubkey(sender_public_key)

    txin_scriptPubKey = CScript(P2PKH_scriptPubKey(sender_address))
    txout_scriptPubKey1 = CScript(P2PKH_scriptPubKey(p1_address))
    txout_scriptPubKey2 = CScript(P2PKH_scriptPubKey(p2_address))
    txout_scriptPubKey3 = CScript(P2PKH_scriptPubKey(p3_address))

    txin = create_txin(txid_to_spend, utxo_index)
    txout1 = create_txout(amount_to_send / 3, txout_scriptPubKey1)
    txout2 = create_txout(amount_to_send / 3, txout_scriptPubKey2)
    txout3 = create_txout(amount_to_send / 3, txout_scriptPubKey3)

    tx = CMutableTransaction([txin], [txout1, txout2, txout3])

    HashSig = SignatureHash(txin_scriptPubKey, tx, 0, SIGHASH_ALL)

    txin.scriptSig = CScript([sender_private_key.sign(HashSig) + bytes([SIGHASH_ALL]), sender_public_key])

    VerifyScript(txin.scriptSig, CScript(txin_scriptPubKey),
                 tx, 0, (SCRIPT_VERIFY_P2SH,))

    return broadcast_transaction(tx, network)


if __name__ == '__main__':
    ######################################################################
    # TODO: set these parameters correctly
    amount_to_send = 0.0003  # amount of BTC in the output you're splitting minus fee
    txid_to_spend = (
        '06647991f96b6ad6ba76f1317a8b949eeece047b8b75bcd9d8ddbf5e40be4b1d')
    utxo_index = 15 # index of the output you are spending, indices start at 0
    ######################################################################


    # txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)
    response = send_from_P2PKH_transaction( amount_to_send, txid_to_spend, utxo_index, p1_address, p2_address, p3_address, my_private_key, network_type)
    print(response.status_code, response.reason)
    print(response.text)