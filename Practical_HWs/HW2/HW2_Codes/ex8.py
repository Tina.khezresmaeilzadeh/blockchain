from bitcoin.core.script import *
from bitcoin.wallet import CBitcoinSecret, P2PKHBitcoinAddress

from lib.utils import *
from lib.config import *



def P2PKH_scriptPubKey(address):
    ######################################################################
    # TODO: Complete the standard scriptPubKey implementation for a
    # PayToPublicKeyHash transaction
    return [
        # fill this in!
        OP_DUP, OP_HASH160, address, OP_EQUALVERIFY, OP_CHECKSIG
    ]
    ######################################################################


def P2PKH_scriptSig(txin, txout, txin_scriptPubKey, private_key, public_key):
    signature = create_OP_CHECKSIG_signature(txin, txout, txin_scriptPubKey,
                                             private_key)
    ######################################################################
    # TODO: Complete this script to unlock the BTC that was sent to you
    # in the PayToPublicKeyHash transaction.
    return [
        # fill this in!
        signature, public_key
    ]
    ######################################################################


def send_from_P2PKH_transaction(amount_to_send, txid_to_spend1, utxo_index1, txid_to_spend2, utxo_index2, txid_to_spend3, utxo_index3, p1_address, p2_address, p3_address, receiver_private_key, network):

    receiver_public_key = receiver_private_key.pub
    receiver_address = P2PKHBitcoinAddress.from_pubkey(receiver_public_key)

    txin_scriptPubKey1 = CScript(P2PKH_scriptPubKey(p1_address))
    txin1 = create_txin(txid_to_spend1, utxo_index1)

    txin_scriptPubKey2 = CScript(P2PKH_scriptPubKey(p2_address))
    txin2 = create_txin(txid_to_spend2, utxo_index2)

    txin_scriptPubKey3 = CScript(P2PKH_scriptPubKey(p3_address))
    txin3 = create_txin(txid_to_spend3, utxo_index3)

    txout_scriptPubKey = CScript(P2PKH_scriptPubKey(receiver_address))
    txout = create_txout(amount_to_send, txout_scriptPubKey)


    tx = CMutableTransaction([txin1, txin2, txin3], [txout])

    HashSig1 = SignatureHash(txin_scriptPubKey1, tx, 0, SIGHASH_ALL)
    txin1.scriptSig = CScript([p1_private_key.sign(HashSig1) + bytes([SIGHASH_ALL]), p1_public_key])

    HashSig2 = SignatureHash(txin_scriptPubKey2, tx, 1, SIGHASH_ALL)
    txin2.scriptSig = CScript([p2_private_key.sign(HashSig2) + bytes([SIGHASH_ALL]), p2_public_key])

    HashSig3 = SignatureHash(txin_scriptPubKey3, tx, 2, SIGHASH_ALL)
    txin3.scriptSig = CScript([p3_private_key.sign(HashSig3) + bytes([SIGHASH_ALL]), p3_public_key])

    VerifyScript(txin1.scriptSig, txin_scriptPubKey1,
                 tx, 0, (SCRIPT_VERIFY_P2SH,))
    VerifyScript(txin2.scriptSig, txin_scriptPubKey2,
                 tx, 1, (SCRIPT_VERIFY_P2SH,))
    VerifyScript(txin3.scriptSig, txin_scriptPubKey3,
                 tx, 2, (SCRIPT_VERIFY_P2SH,))

    return broadcast_transaction(tx, network)


if __name__ == '__main__':
    ######################################################################
    # TODO: set these parameters correctly
    amount_to_send = 0.000001
    txid_to_spend1 = (
        '7a5881ca8dc0dd7ed565c7eedba05bc755ce57190257f748ed72bf67130b0010')
    utxo_index1 = 0  # index of the output you are spending, indices start at 0
    txid_to_spend2 = (
        '7a5881ca8dc0dd7ed565c7eedba05bc755ce57190257f748ed72bf67130b0010')
    utxo_index2 = 1
    txid_to_spend3 = (
        '7a5881ca8dc0dd7ed565c7eedba05bc755ce57190257f748ed72bf67130b0010')
    utxo_index3 = 2
    ######################################################################


    # txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)
    response = send_from_P2PKH_transaction( amount_to_send, txid_to_spend1, utxo_index1, txid_to_spend2, utxo_index2, txid_to_spend3, utxo_index3, p1_address,
        p2_address,
        p3_address,
        my_private_key,
        network_type
    )
    print(response.status_code, response.reason)
    print(response.text)