from bitcoin import SelectParams
from bitcoin.base58 import decode
from bitcoin.core import x
from bitcoin.wallet import CBitcoinAddress, CBitcoinSecret, P2PKHBitcoinAddress


SelectParams('testnet')

faucet_address = CBitcoinAddress('mv4rnyY3Su5gjcDNzbMLKBQkBicCtHUtFB')

# For questions 1-3, we are using 'btc-test3' network. For question 4, you will
# set this to be either 'btc-test3' or 'bcy-test'
network_type = 'btc-test3'


######################################################################
# This section is for Questions 1-3
# TODO: Fill this in with your private key.
#
# Create a private key and address pair in Base58 with keygen.py
# Send coins at https://coinfaucet.eu/en/btc-testnet/

my_private_key = CBitcoinSecret(
    'cV8Z9iBRGzzyQKhfZc9m1stWgnz8gt14H3aWvhsp1we6kY33f8X4')

my_public_key = my_private_key.pub
my_address = P2PKHBitcoinAddress.from_pubkey(my_public_key)
print('my address is: ', my_address)
######################################################################


######################################################################
# NOTE: This section is for Question 4
# TODO: Fill this in with address secret key for BTC testnet3
#
# Create address in Base58 with keygen.py
# Send coins at https://coinfaucet.eu/en/btc-testnet/

# Only to be imported by alice.py
# Alice should have coins!!
alice_secret_key_BTC = CBitcoinSecret(
    'cP29pvVAq8xv4vCmbLoNtJsgcUMZxGFY9HPBn9c3sRjJkPemj3c8')

# Only to be imported by bob.py
bob_secret_key_BTC = CBitcoinSecret(
    'cVHSjpp7tnWBPXbRWyWHpat1Gi1yH5cgtvxJnVEjoSWRkasAysJJ')

# Can be imported by alice.py or bob.py
alice_public_key_BTC = alice_secret_key_BTC.pub
alice_address_BTC = P2PKHBitcoinAddress.from_pubkey(alice_public_key_BTC)
print('alice: ', alice_address_BTC)
bob_public_key_BTC = bob_secret_key_BTC.pub
bob_address_BTC = P2PKHBitcoinAddress.from_pubkey(bob_public_key_BTC)
print('bob: ', bob_address_BTC)
######################################################################
# Question 3 keys
faraz_private_key = CBitcoinSecret(
    'cMpnABZ4wAYFsqxf7KEhaeMRnsL1fKfHvALNY3gYSqoZc2WCWkqZ')

faraz_public_key = faraz_private_key.pub
faraz_address = P2PKHBitcoinAddress.from_pubkey(faraz_public_key)

ata_private_key = CBitcoinSecret(
    'cMnYhYny9sNzbjES25afeqDmG7nVHRsx8APQ2RxbmaiCTGAxzZXQ')

ata_public_key = faraz_private_key.pub
ata_address = P2PKHBitcoinAddress.from_pubkey(faraz_public_key)


p1_private_key = CBitcoinSecret(
    'cTJAT2eTyxtatMSPUGfW9xhWQPP9Gbj8ZNhEkoTZn35JS3jmsC8G')

p1_public_key = p1_private_key.pub
p1_address = P2PKHBitcoinAddress.from_pubkey(p1_public_key)



p2_private_key = CBitcoinSecret(
    'cRDaH7gsNJNMxb916Vp9DUDyaSUDZfb5HHdjBLmDo4iNjHYgrPyQ')
p2_public_key = p2_private_key.pub
p2_address = P2PKHBitcoinAddress.from_pubkey(p2_public_key)



p3_private_key = CBitcoinSecret(
    'cVgj3oDQ8hWCGs1LGX2zxEHNsSYtvAKYJrawwi479cKCvPjdjGHv')

p3_public_key = p3_private_key.pub
p3_address = P2PKHBitcoinAddress.from_pubkey(p3_public_key)


p4_private_key = CBitcoinSecret(
    'cT3J3wjfLDGdmwFMXzCGpg5vMVb3P1pJ1CmSeY6KV8KAvynZmZsa')

p4_public_key = p4_private_key.pub
p4_address = P2PKHBitcoinAddress.from_pubkey(p4_public_key)


p5_private_key = CBitcoinSecret(
    'cRNPMcWiNtkchVSChHtWcedFZ49Q7xPjNqRFUpwgwR3oaKjpmAKS')

p5_public_key = p5_private_key.pub
p5_address = P2PKHBitcoinAddress.from_pubkey(p5_public_key)

######################################################################
# NOTE: This section is for Question 4
# TODO: Fill this in with address secret key for BCY testnet
#
# Create address in hex with
# curl -X POST https://api.blockcypher.com/v1/bcy/test/addrs?token=$YOURTOKEN
# This request will return a private key, public key and address. Make sure to save these.
#
# Send coins with
# curl -d '{"address": "BCY_ADDRESS", "amount": 1000000}' https://api.blockcypher.com/v1/bcy/test/faucet?token=<YOURTOKEN>
# This request will return a transaction reference. Make sure to save this.

# Only to be imported by alice.py
alice_secret_key_BCY = CBitcoinSecret.from_secret_bytes(
    x('81f46c3cff0c86799230fac5ef41c91d876612098527fa46c257a20142246e8e'))

'''
"private": "81f46c3cff0c86799230fac5ef41c91d876612098527fa46c257a20142246e8e",
  "public": "029615a18360ccdeca679a392cd42474d31950acd0d3e540536ff3c2cef48e041d",
  "address": "C5ioMPVbmast3ZvKQDv9cDZwwDjoBNCbbD",
  "wif": "BsgeQzTrRU66noKvmZB8pRYPxnFZdj3aiGyVJjcsAuHVJs5zU68X"
'''
# Only to be imported by bob.py
# Bob should have coins!!
bob_secret_key_BCY = CBitcoinSecret.from_secret_bytes(
    x('aac320e40bae55d8704f58ec8765cb9f30dc264c3cc0d131d39c3af4adfdde68'))

'''
"private": "aac320e40bae55d8704f58ec8765cb9f30dc264c3cc0d131d39c3af4adfdde68",
  "public": "028124f9b9231c2f35b80e41ecd6a827af816b317b7ca9f4657b8e35e1dea33c2f",
  "address": "CDhPXGjfSk4HpiZ4hRfv76oPh7h5upsT34",
  "wif": "Bu3yE4qmQe8FRpegN8GmikjALf5CzeL8MF11WNkggTxkcyKBAGGq"
'''


# Can be imported by alice.py or bob.py
alice_public_key_BCY = alice_secret_key_BCY.pub
alice_address_BCY = P2PKHBitcoinAddress.from_pubkey(alice_public_key_BCY)

bob_public_key_BCY = bob_secret_key_BCY.pub
bob_address_BCY = P2PKHBitcoinAddress.from_pubkey(bob_public_key_BCY)
######################################################################
