from sys import exit
from bitcoin.core.script import *

from lib.utils import *
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from ex1 import send_from_P2PKH_transaction


######################################################################
# TODO: Complete the scriptPubKey implementation for Exercise 2

Q2a_txout_scriptPubKey = [OP_RETURN, 0x3539353130313639686564617A6C6965616D73452D727A65684B]

######################################################################

if __name__ == '__main__':
    ######################################################################
    # TODO: set these parameters correctly
    amount_to_send = 0.0003 # amount of BTC in the output you're splitting minus fee
    txid_to_spend = (
        '06647991f96b6ad6ba76f1317a8b949eeece047b8b75bcd9d8ddbf5e40be4b1d')
    utxo_index = 4 # index of the output you are spending, indices start at 0
    ######################################################################

    response = send_from_P2PKH_transaction(
        amount_to_send, txid_to_spend, utxo_index,
        Q2a_txout_scriptPubKey, my_private_key, network_type)
    print(response.status_code, response.reason)
    print(response.text)
