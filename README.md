﻿## Blockchain 

This course was taught by Dr. Maddah-Ali at the Department of Electrical Engineering.

 - **Practical Homework 1:** Discrete Logarithm-Merkle Tree-Implementing LCR Algorithm, Hash sha256
 - **Practical Homework 2:** Bitcoin Script Programming
 - **Practical Homework 3:** Solidity Programming



